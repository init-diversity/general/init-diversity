## init-diversity
-----------------------

## Summary
---------------------------------

A metapackage with a collection of software that can configure antiX or any Debian based system to use multiple inits.
This package can be safely removed after the software it contains is installed.




## Recommended build instructions
---------------------------------

`gbp clone https://gitlab.com/init-diversity/general/init-diversity.git && cd init-diversity && gbp buildpackage`

is the recommended method to build this package directly from git.


`sudo apt install git-buildpackage`

should get you all the software required to build using this method.



